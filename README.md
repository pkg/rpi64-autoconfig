# Apertis autoconfiguration for RPi64

A set of files and scripts to automatically configure Apertis on system startup
without requiring user interaction.
